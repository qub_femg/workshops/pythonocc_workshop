# Pythonocc Workshop
## Introduction
This is a repository for a workshop on the **PythonOCC** CAD system. The workshop uses a Jupyter Notebook to run and explain the different Python code involved with using PythonOCC.

## Setup
Before anything can be run, you must install Python, PythonOCC and Jupyter Notebook including all the other relevant Python packages.

First if you do not already by **Miniconda** or **Anaconda** installed for package management then you will have to install Miniconda. We will use Miniconda instead of Anaconda as we only need the package management. Miniconda can be installed from here: https://docs.conda.io/en/latest/miniconda.html

Next open your Anaconda prompt (Windows) or terminal (Linux). We will now create a new conda environment with Python 3.8 and Numpy. Set the variable "your_environment_name" to whatever you want.

```bash
$ conda create -n your_environment_name python=3.8 numpy
```

We can activate our newly created conda environment, by typing:

```bash
$ conda activate your_environment_name
```

Once you are in your conda environment. We can check what Python packages are installed in our environment by running:

```bash
$ conda list
```

Next we will install PythonOCC using the following command:

```bash
$ conda install -c conda-forge pythonocc-core=7.5.1
```

There have been issues with this version of pythonocc on Windows. If it won't install, then you have have to install it from a different source and downgrade Python.

```bash
$ conda install python=3.7
$ conda install -c dlr-sc pythonocc-core
```

To check that PythonOCC is installed ok as well as the other packages we have installed you can run:

```bash
$ python
>>> import numpy as np
>>> import OCC
```

If an error occurs then there was a problem with the installation of the respective package.

```bash
>>> exit()
```

Use the above command to exit python.

Lastly, we will install Jupyter Notebook and a package to allow us to display PythonOCC within Jupyter:

```bash
$ conda install -c conda-forge notebook
$ conda install -c conda-forge pythreejs
```

You can now either git clone this repo (use the HTTPS address not SSH) or download it as a zip to start the notebook.

Use the "cd" command in the prompt to move to where you have the repo stored. "dir" (Windows) or "ls" (Linux) can help you see what files and dirs are in the current directory you are located in. To open Jupyter Notebook, in your prompt type:

```bash
$ jupyter notebook
```
You should see Jupyter notebook in your web browser. If not follow the web address in the prompt (i.e. www.localhost:8888....).

"Shift+Enter" will allow you to run each cell individually in the notebook.
